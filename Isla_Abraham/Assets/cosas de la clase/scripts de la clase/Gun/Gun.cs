using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject Prefab_bullet;
    public Transform Spawn_point;
    public float force;

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject go = Instantiate(Prefab_bullet, Spawn_point.position, Spawn_point.rotation);
            go.GetComponent<Rigidbody>().AddForce(Spawn_point.forward * force, ForceMode.Impulse);

            Destroy(go, 5f);
        }
    }
}
