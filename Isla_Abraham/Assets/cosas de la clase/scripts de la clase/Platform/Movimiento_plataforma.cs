using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_plataforma : MonoBehaviour
{
    public GameObject Plataforma;
   
    public Transform MinPosition;
    public Transform MaxPosition;

    public float SpeedMovement;

    private void OnTriggerStay(Collider other)
    {

        if (other != null )
        {

            MovePlataform();

        }

    }

    private void MovePlataform() 
    {

        Plataforma.transform.Translate(Vector3.up * Time.deltaTime * SpeedMovement);


        if (Plataforma.transform.position.y >= MaxPosition.transform.position.y)
        {
            SpeedMovement = -5f;
        }
        if (Plataforma.transform.position.y <= MinPosition.transform.position.y)
        {
            SpeedMovement = 5f;
        }

    }
}
