using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MovementController))]

public class Enemy_controller : MonoBehaviour
{
    public MovementController movement;
    public Transform Jugador;
    public float DetectionRange;
    public LayerMask CapaJugador;
    public bool EstaAlerta;
    public Transform Player;



    private void Update()
    {
        EstaAlerta = Physics.CheckSphere(transform.position, DetectionRange, CapaJugador);

        

        if (EstaAlerta == true)
        {
                        
            /*Vector3 forward = transform.transform.TransformDirection(Vector3.forward.normalized);
            Vector3 toOther = transform.position - Jugador.transform.position.normalized;

            if (Vector3.Dot(forward, toOther) < 1)
            {
                movement.rotate(1);

                print("kappa");
            }
            if (Vector3.Dot(forward, toOther) > 1)
            {
                movement.rotate(1);

                print("kappa");
            }*/
            transform.LookAt(Player);
            movement.Move(-1, 0);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, DetectionRange);
    }
}
/*
    
     private void Update()
    {
        float distance = Vector3.Distance(transform.position, Jugador.transform.position);

        if (distance <= DetectionRange)
        {
            
        }
    }
 
    
    }*/