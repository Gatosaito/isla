using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Player_interactions : MonoBehaviour
{
    public player_stadistic Player_estadisticas;

    public Transform cameraPlayer;
    public Transform objeto_vacio_Caja;
    public Transform objeto_Vacio_Arma;
    public LayerMask lm;
    public float Ray_distance;

    private void Update()
    {
        if (Input.GetButtonDown("Agarrar"))
        {
            if (objeto_vacio_Caja.childCount > 0)
            {
                objeto_vacio_Caja.GetComponentInChildren<Rigidbody>().isKinematic = false;
                objeto_vacio_Caja.DetachChildren();

                if (objeto_Vacio_Arma.childCount > 0)
                {
                    objeto_Vacio_Arma.GetChild(0).gameObject.SetActive(true);
                }
            }
            else
            {
                Debug.DrawRay(cameraPlayer.position, cameraPlayer.forward * Ray_distance, Color.black);
                if (Physics.Raycast(cameraPlayer.position, cameraPlayer.forward, out RaycastHit hit, Ray_distance, lm))
                {
                    hit.transform.GetComponentInChildren<Rigidbody>().isKinematic = true;
                    hit.transform.parent = objeto_vacio_Caja;
                    hit.transform.localPosition = Vector3.zero;
                    hit.transform.localRotation = Quaternion.Euler(Vector3.zero);
                    if (objeto_Vacio_Arma.childCount > 0)
                    {
                        objeto_Vacio_Arma.GetChild(0).gameObject.SetActive(false);
                    }
                }
            }
        }
    }  
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Door" && Player_estadisticas.BateriCont >= 3)
        {
            other.GetComponentInParent<Puerta>().OnOpenDoor();
        }

        if (other.tag == "Batery")
        {

            other.GetComponentInParent<Pato>().Duck_disappear();
            Player_estadisticas.BateriCont++;
        }
        if (other.tag == "Gun")
        {
            other.transform.parent = objeto_Vacio_Arma;
            other.transform.localPosition = Vector3.zero;
            other.transform.localRotation = Quaternion.Euler(0, 0, 0);

            if (objeto_vacio_Caja.childCount > 0)
            {
                objeto_Vacio_Arma.GetChild(0).gameObject.SetActive(false);
            }
        }
    }
}
