using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    [Header("Movimiento de personaje")]
    public float speed_movement;
    public Vector3 direccion;
    public CharacterController controller;

    [Header("Gravedad")]
    public float jump;
    public Vector3 movement_Y;
    public float gravity = -9.8f;

    [Header("rotacion")]
    public float rotacion_playerY;

    private void Update()
    {
        //salto
        movement_Y.y += gravity * Time.deltaTime;

        controller.Move(movement_Y * Time.deltaTime);

        if (controller.isGrounded && movement_Y.y < 0)
        {
            movement_Y.y = -2f;
        }


        if (controller.isGrounded && Input.GetButtonDown("Jump"))
        {
            movement_Y.y = Mathf.Sqrt(jump * -2 * gravity);
        }

    }

    public void Move(float Vertical, float Horizontal) 
    {
        //movimiento del enemigo por el analogo 
        direccion.x = Horizontal;

        direccion.z = Vertical;

        //Movimiento de personaje 
        direccion = transform.TransformDirection(direccion);

        controller.Move(direccion * Time.deltaTime * speed_movement);
    }

    public void rotate(float rotatevalue) 
    {
        rotacion_playerY += rotatevalue;
        //rotacion de camara
        controller.transform.rotation = Quaternion.Euler(0, rotacion_playerY, 0);
    }
}