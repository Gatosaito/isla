using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MovementController))]
public class PlayerController : MonoBehaviour
{
    [Header("Rotacion de camera")]
    public Vector2 mouse_movement;
    public Camera player_camera;
    public float rotacionCamera;
    public float SensibilidadMouse;

    [Header("Movement Controller")]
    public MovementController movement;
    public MovementController rotate;

    private void Update()
    {
        mouse_movement.x = Input.GetAxis("Mouse X") * SensibilidadMouse;
        mouse_movement.y = Input.GetAxis("Mouse Y") * SensibilidadMouse;
    

        rotacionCamera -= mouse_movement.y;
        

        rotacionCamera = Mathf.Clamp(rotacionCamera, -85, 85);

        player_camera.transform.localRotation = Quaternion.Euler(rotacionCamera, 0, 0);

        movement.Move(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));

        rotate.rotate(Input.GetAxis("Mouse X"));
    }
}
