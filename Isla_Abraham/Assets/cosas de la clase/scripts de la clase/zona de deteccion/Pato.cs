using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pato : MonoBehaviour
{
    public AudioSource Cuack;
    public AudioClip Sonido;
    public GameObject Sensor;

    public void Duck_disappear()
    {
        AudioSource.PlayClipAtPoint(Sonido, gameObject.transform.position);
        Destroy(gameObject);
    }
}
/*Sensor.SetActive(false);*/
