using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public Animator animacion;
    
    public void OnOpenDoor() 
    {
        animacion.SetTrigger("Trigger_puerta");
    }
}
